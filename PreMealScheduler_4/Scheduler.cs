﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data;
namespace PreMealScheduler_4
{
    class Scheduler
    {
        public static StreamReader fileStreamReader;
        public static DataTable fileDataTable = new DataTable();
        public static string connstring = ConfigurationSettings.AppSettings["azureDBConnectionstring"].ToString();
        /// <summary>
        /// This is the entry point of the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            LogProcess("-------------------------Scheduler #4 Begins @ " + DateTime.Now.ToString() + "----------------------");
            StartProcess();
            LogProcess("-------------------------Scheduler #4 Ends @ " + DateTime.Now.ToString() + "-----------------------" +
                Environment.NewLine);
        }
        /// <summary>
        /// This is the function where scheduler #4 process occurs
        /// </summary>
        public static void StartProcess()
        {
            try
            {
                //Process ONLY when file exist in the Azure File Storage Location
                if (CheckIfNewFileExist())
                {
                    LogProcess("input file named mailclean.csv found");
                    //Read the input file
                    ReadFile();
                    LogProcess("File Data is Read and loaded into Data Table");
                    //Delete the input file once processing is done
                    DeleteFile();
                    LogProcess("File is deleted from Azure storage location");
                    //Populate selection table with file data
                    PopulateSelectionTable();
                }
                else
                {
                    LogProcess("No input file named mailclean.csv NOT found in the azure file storage");
                }
            }
            catch (Exception e)
            {
                LogProcess("Exception while executing function " + new StackTrace(e).GetFrame(0).GetMethod().Name +
                    "with the Error " + e.Message);
                //Log error in table
                LogError(e.Message, new StackTrace(e).GetFrame(0).GetMethod().Name);
            }
        }
        /// <summary>
        /// This function is used to check if new input file exist in azure storage location
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfNewFileExist()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            //upload file data into stream reader
                            fileStreamReader = new StreamReader(file.OpenRead());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                            return true;

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }
        /// <summary>
        /// This function logs the steps in the process in log.txt file in azure storage location
        /// </summary>
        /// <param name="message">message to be logged</param>
        public static void LogProcess(string message)
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["LogFile"].ToString());
                        if (file.Exists())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(file.DownloadText());
                            sb.Append(Environment.NewLine);
                            sb.Append(message);
                            file.UploadText(sb.ToString());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function logs the error to table
        /// </summary>
        /// <param name="error">error to be logged in table</param>
        /// <param name="method">message threw error</param>
        public static void LogError(string error, string method)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("InsertLog", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@date", SqlDbType.VarChar).Value = DateTime.Now.ToString();
                cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = error;
                cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = method;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to get a sql connection for future use
        /// </summary>
        /// <param name="conn">DB Connection string</param>
        /// <returns>a sql connection object</returns>
        private static SqlConnection GetSqlConnection(string conn)
        {
            try
            {
                var connection = new SqlConnection(conn);
                connection.Open();
                return connection;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function reads the input file and loads data table
        /// </summary>
        public static void ReadFile()
        {
            try
            {
                string line = string.Empty;
                fileDataTable.Columns.Add("Email_id");
                //  Read the file
                string[] columns;
                while ((line = fileStreamReader.ReadLine()) != null)
                {
                    columns = line.Replace(" ", "").Split(new string[] { "," }, StringSplitOptions.None);
                    fileDataTable.Rows.Add(columns);
                }
                line = null;
                columns = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This funcion is used to load the selection table and customer table
        /// </summary>
        public static void PopulateSelectionTable()
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("PopulateCustomer", connection);
            try
            {
                SqlBulkCopy bulkcopy = new SqlBulkCopy(connstring);
                bulkcopy.DestinationTableName = ConfigurationSettings.AppSettings["MainTable"].ToString();
                bulkcopy.WriteToServer(fileDataTable);
                LogProcess("Data from Data table is loaded into TMP_SelCustomer Table");
                bulkcopy.Close();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                LogProcess("Data from tmp_premeal is loaded into pm_customer Table");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function deletes the input file once processing is done
        /// </summary>
        public static void DeleteFile()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            file.Delete();
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
